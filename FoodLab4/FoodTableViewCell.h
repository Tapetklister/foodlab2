//
//  FoodTableViewCell.h
//  FoodLab4
//
//  Created by Anton Nilsson on 2016-03-30.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewCell : UITableViewCell

@property NSString *foodNumber;

@end
