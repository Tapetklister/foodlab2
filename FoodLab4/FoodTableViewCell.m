//
//  FoodTableViewCell.m
//  FoodLab4
//
//  Created by Anton Nilsson on 2016-03-30.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "FoodTableViewCell.h"

@implementation FoodTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
