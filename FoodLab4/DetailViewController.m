//
//  DetailViewController.m
//  FoodLab4
//
//  Created by Anton Nilsson on 2016-03-30.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndocator;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *label4;
@property (weak, nonatomic) IBOutlet UILabel *label5;
@property (weak, nonatomic) IBOutlet UILabel *label6;
@property (weak, nonatomic) IBOutlet UILabel *label7;
@property (weak, nonatomic) IBOutlet GKBarGraph *graph;
@property (weak, nonatomic) IBOutlet UILabel *grade;
@property NSArray *labelArray;
@property NSDictionary *result;
@property NSArray *nutrients;
@property NSArray *nutrientValues;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.activityIndocator startAnimating];
    
    NSLog(@"%@", self.foodNumber);
    
    self.graph.dataSource = self;
    
    self.result = [[NSDictionary alloc]init];
    self.nutrients = [[NSArray alloc]init];
    self.nutrientValues = [[NSArray alloc]init];
    self.labelArray = [[NSArray alloc]initWithObjects:
                       self.label1,
                       self.label2,
                       self.label3,
                       self.label4,
                       self.label5,
                       self.label6,
                       self.label7,
                       nil];
    
    NSString *urlString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.foodNumber];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            [self alert:@"Could not retrieve data. Please check your internet connection"];
            return;
        }
        NSError *parseError;
        
        self.result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if(parseError) {
            [self alert:@"Could not parse data"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Result: %@", self.result);
            self.nutrients = @[@"protein", @"fat", @"vitaminB12", @"vitaminB6", @"vitaminC", @"vitaminD", @"vitaminE"];
            self.nutrientValues = [[NSArray alloc]initWithObjects:
                                   self.result[@"nutrientValues"][self.nutrients[0]],
                                   self.result[@"nutrientValues"][self.nutrients[1]],
                                   self.result[@"nutrientValues"][self.nutrients[2]],
                                   self.result[@"nutrientValues"][self.nutrients[3]],
                                   self.result[@"nutrientValues"][self.nutrients[4]],
                                   self.result[@"nutrientValues"][self.nutrients[5]],
                                   self.result[@"nutrientValues"][self.nutrients[6]],
                                   nil];
            
            self.label1.text = [NSString stringWithFormat:@"Protein: %@", self.result[@"nutrientValues"][@"protein"]];
            self.label2.text = [NSString stringWithFormat:@"Fett: %@", self.result[@"nutrientValues"][@"fat"]];
            self.label3.text = [NSString stringWithFormat:@"Vitamin B12: %@", self.result[@"nutrientValues"][@"vitaminB12"]];
            self.label4.text = [NSString stringWithFormat:@"Vitamin B6: %@", self.result[@"nutrientValues"][@"vitaminB6"]];
            self.label5.text = [NSString stringWithFormat:@"Vitamin C: %@", self.result[@"nutrientValues"][@"vitaminC"]];
            self.label6.text = [NSString stringWithFormat:@"Vitamin D: %@", self.result[@"nutrientValues"][@"vitaminD"]];
            self.label7.text = [NSString stringWithFormat:@"Vitamin E: %@", self.result[@"nutrientValues"][@"vitaminE"]];
            
            [self loopAnimation:self.label1];
            [self loopAnimation:self.label2];
            [self loopAnimation:self.label3];
            [self loopAnimation:self.label4];
            [self loopAnimation:self.label5];
            [self loopAnimation:self.label6];
            [self loopAnimation:self.label7];
            
            [self gradeNutrients];
            
            [self.graph draw];
            self.activityIndocator.hidden = YES;
        });
    }];
    
    [task resume];
}

-(void) gradeNutrients {
    
    NSNumber *proteinValues = [NSNumber numberWithInteger:[self.result[@"nutrientValues"][self.nutrients[0]]intValue]*2];
    NSLog(@"Protein: %@", proteinValues);
    NSNumber *fatValue = [NSNumber numberWithInteger:[self.result[@"nutrientValues"][self.nutrients[1]]intValue]];
    NSLog(@"Fat: %@", fatValue);
    
    if(fatValue>proteinValues) {
        self.grade.text = @"Onyttig";
        self.grade.textColor = [UIColor redColor];
    } else {
        self.grade.text = @"Nyttig";
        self.grade.textColor = [UIColor blueColor];
    }
}

-(void) loopAnimation: (UILabel *) label{
    
    float xPos = self.view.center.x;
    
    [UIView animateWithDuration:1.0 delay:0 options:kNilOptions animations:^{
        label.center = CGPointMake(label.center.x, label.center.y);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 delay:0 options:kNilOptions animations:^{
            label.center = CGPointMake(xPos-10, label.center.y);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0 delay:0 options:kNilOptions animations:^{
                label.center = CGPointMake(xPos+10, label.center.y);
            } completion:^(BOOL finished) {
                [self loopAnimation:label];
            }];

        }];
    }];
}

-(NSInteger)numberOfBars {
    return self.nutrients.count;
}

-(NSNumber *)valueForBarAtIndex:(NSInteger)index {
    NSLog(@"valeForBarAtIndex: %@", self.result[@"nutrientValues"][self.nutrients[index]]);
    NSNumber *returnNumber = [NSNumber numberWithInteger:[self.result[@"nutrientValues"][self.nutrients[index]]intValue]*3];
    NSLog(@"Return number: %@", returnNumber);
    return returnNumber;
}

-(NSString *)titleForBarAtIndex:(NSInteger)index {
    return @"";
}

-(UIColor *) colorForBarAtIndex:(NSInteger)index {
    
    return [self.labelArray[index] backgroundColor];
}

-(void) alert:(NSString *) alertText {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:alertText preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        exit(1);
    }];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
