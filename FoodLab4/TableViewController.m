//
//  TableViewController.m
//  FoodLab4
//
//  Created by Anton Nilsson on 2016-03-30.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "TableViewController.h"
#import "FoodTableViewCell.h"
#import "DetailViewController.h"

@interface TableViewController ()
@property (nonatomic) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) NSArray *result;
@property (nonatomic) NSArray *filteredResult;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.activityIndicator startAnimating];
    self.result = [[NSArray alloc]init];
    self.filteredResult = [[NSArray alloc]init];
    
    NSURL *url = [NSURL URLWithString:@"http://matapi.se/foodstuff?query="];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error) {
            [self alert:@"Could not retrieve data. Please check your internet connection"];
            return;
        }
        NSError *parseError;
        self.result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        if(parseError) {
            [self alert:@"Could not parse data"];
        }
        
        NSLog(@"%@", self.result);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self filter];
            self.activityIndicator.hidden = YES;
        });
        
    }];
    
    [task resume];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
}

-(void) alert:(NSString *) alertText {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:alertText preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        exit(1);
    }];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void) updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains [c] %@", self.searchController.searchBar.text];
    self.filteredResult = [self.result filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

-(void) filter {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains [c] %@", self.searchController.searchBar.text];
    self.filteredResult = [self.result filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.searchController.isActive && self.searchController.searchBar.text.length>0) {
        return self.filteredResult.count;
    } else {
        return self.result.count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSArray *activeArray;
    
    if(self.searchController.isActive && self.searchController.searchBar.text.length>0) {
        activeArray = self.filteredResult;
    } else {
        activeArray = self.result;
    }
    
    cell.textLabel.text = activeArray[indexPath.row][@"name"];
    cell.foodNumber = activeArray[indexPath.row][@"number"];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    FoodTableViewCell *cell = sender;
    DetailViewController *detailViewController = [segue destinationViewController];
    detailViewController.title = cell.textLabel.text;
    detailViewController.foodNumber = cell.foodNumber;
    
}


@end
