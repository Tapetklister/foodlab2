//
//  ViewController.m
//  FoodLab4
//
//  Created by Anton Nilsson on 2016-03-30.
//  Copyright © 2016 Anton Nilsson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIDynamicItemBehavior *bounce;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)screenTap:(UITapGestureRecognizer *)sender {
    
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc]initWithItems:@[self.button]];
    self.collision = [[UICollisionBehavior alloc]initWithItems:@[self.button]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    self.bounce = [[UIDynamicItemBehavior alloc]initWithItems:@[self.button]];
    self.bounce.elasticity = 1;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    [self.animator addBehavior:self.bounce];
    
}

@end
